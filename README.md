# kontrollliste

Filtert eine Liste von Aleph Sequential-Einträgen nach spezifischen Feld-Unterfeld-Kombinationen.

## Installation

[Rust](https://rust-lang.org) und dessen Paketmanager Cargo müssen
installiert sein. Anschliessend

- Repository klonen und ins erstellte Verzeichnis wechseln
- `cargo build --release`
- Die ausführbare Binärdatei (`kontrollliste`) befindet sich nun im Verzeichnis
  `target/release`

## Gebrauch

Aufruf:

```
./kontrolliste pfad/zur/filterdatei < pfad/zur/sequentials/datei
```

Die Filter werden in einer separaten Datei nach folgendem Muster definiert:

```
filter,feld,unterfeld1,unterfeld2,...
```

__`filter`__ ist eines der folgenden Zeichen:

- `-`: Die aufgeführten Unterfelder dürfen nicht im Feld vorkommen (_blacklist_)
- `+`: Nur die aufgeführten Unterfelder dürfen im Feld vorkommen (_whitelist_)
- `=`: Feld existiert (Unterfelder sind hier nicht notwendig)

__`feld`__ sollte zwischen drei (nur Feld) und fünf Zeichen (Feld und Indikatoren) lang sein.
Ist `feld` weniger als fünf Zeichen lang, werden Stellen bis fünf mit Leerzeichen aufgefüllt.
Bspw.: `100` -> `100  `. Ein `?` dient als Wildcard für ein Zeichen, doch kein Leerzeichen.
So matcht bspw. `100??` `10001` und `100AB`, aber nicht `100__` (`_` steht für ein Leerzeichen).

__`unterfeld`__ ist genau ein Zeichen lang (d.h. ohne führendes `$`). Mehrere Unterfelder werden
mit Komma abgetrennt.

Ausgegeben werden alle Felder, auf welche die Regel zutreffen. Im Fall von einem Filter
`-,60010,a,b` würde also bspw. folgende Zeile ausgegeben werden:

```
000000794 60010 L $$aCicero, Marcus Tullius$$xManuscripts.
```

Hingegen folgende nicht:

```
000000794 24514 L $$aThe descent of manuscripts.
```

Die Ausgabe besteht jeweils aus _identifier_, Feld und Indikatoren sowie Inhalt der Unterfelder.
Sie ist sortiert nach Feld-Indikator-Kombination, dann _identifier_. Leere Indikatoren werden zur
Verdeutlichung mit `_` angegeben.
