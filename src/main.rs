//! # kontrollliste
//!
//! Filtert eine Liste von Aleph Sequential-Einträgen nach spezifischen Feld-Unterfeld-Kombinationen.
//! Aufruf:
//!
//! ```
//! ./kontrolliste pfad/zur/filterdatei < pfad/zur/sequentials/datei
//! ```
//!
//! Die Filter werden in einer separaten Datei nach folgendem Muster definiert:
//!
//! ```
//! filter,feld,unterfeld1,unterfeld2,...
//! ```
//!
//! __`filter`__ ist eines der folgenden Zeichen:
//!
//! - `-`: Die aufgeführten Unterfelder dürfen nicht im Feld vorkommen (_blacklist_)
//! - `+`: Nur die aufgeführten Unterfelder dürfen im Feld vorkommen (_whitelist_)
//! - `=`: Feld existiert (Unterfelder sind hier nicht notwendig)
//!
//! __`feld`__ sollte zwischen drei (nur Feld) und fünf Zeichen (Feld und Indikatoren) lang sein.
//! Ist `feld` weniger als fünf Zeichen lang, werden Stellen bis fünf mit Leerzeichen aufgefüllt.
//! Bspw.: `100` -> `100  `. Ein `?` dient als Wildcard für ein Zeichen, doch kein Leerzeichen.
//! So matcht bspw. `100??` `10001` und `100AB`, aber nicht `100__` (`_` steht für ein Leerzeichen).
//!
//! __`unterfeld`__ ist genau ein Zeichen lang (d.h. ohne führendes `$`). Mehrere Unterfelder werden
//! mit Komma abgetrennt.
//!
//! Ausgegeben werden alle Felder, auf welche die Regel zutreffen. Im Fall von einem Filter
//! `-,60010,a,b` würde also bspw. folgende Zeile ausgegeben werden:
//!
//! ```
//! 000000794 60010 L $$aCicero, Marcus Tullius$$xManuscripts.
//! ```
//!
//! Hingegen folgende nicht:
//!
//! ```
//! 000000794 24514 L $$aThe descent of manuscripts.
//! ```
//!
//! Die Ausgabe besteht jeweils aus _identifier_, Feld und Indikatoren sowie Inhalt der Unterfelder.
//! Sie ist sortiert nach Feld-Indikator-Kombination, dann _identifier_. Leere Indikatoren werden zur
//! Verdeutlichung mit `_` angegeben.
//!

use std::cmp::Ordering;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Lines;
use std::io::Read;
use std::path::Path;
use std::prelude::v1::Vec;
use std::result::Result::Ok;

#[derive(PartialEq)]
enum ListType {
    Whitelist,
    Blacklist,
    NonExistence,
}

struct Rule {
    list_type: ListType,
    field: String,
    subfields: Vec<char>,
}

impl Rule {
    fn new(s: &str) -> Rule {
        let mut splitted = s.split(",");
        let list_type = match splitted.next().unwrap() {
            "+" => ListType::Whitelist,
            "-" => ListType::Blacklist,
            _ => ListType::NonExistence,
        };
        let field = format!("{:<5}", splitted.next().unwrap());
        let subfields = if list_type != ListType::NonExistence {
            splitted.map(|x| x.chars().next().unwrap())
                .collect::<Vec<char>>()
        } else {
            vec![]
        };
        Rule {
            list_type,
            field,
            subfields,
        }
    }
}

struct MarcField {
    line: String,
    subfields: Option<Vec<char>>,
}

impl MarcField {
    fn new(s: &str) -> MarcField {
        let splitted = s[18..].split("$$");
        let subfields = if splitted.clone().count() >= 2 {
            Some(splitted.skip(1)
                .map(|x| x.chars().next())
                .filter(|x| x.is_some())
                .map(|x| x.unwrap())
                .collect::<Vec<char>>()
            )
        } else {
            None
        };
        MarcField {
            line: s.to_string(),
            subfields,
        }
    }

    fn is_field(&self, field: &str) -> bool {
        field.chars()
            .zip(self.line[10..15].chars())
            .all(|(a, b)| match a {
                '?' => b != ' ',
                _ => a == b,
            })
    }

    /// Returns true if no char in blacklist is found in subfields
    fn blacklist_check(&self, blacklist: &Vec<char>) -> bool {
        match self.subfields {
            Some(ref s) => blacklist.iter()
                    .all(|x| s.iter().all(|y| *y != *x)),
            None => blacklist.len() > 0
        }
    }

    /// Returns true if no char except the ones in whitelist is found in subfield
    fn whitelist_check(&self, whitelist: &Vec<char>) -> bool {
        match self.subfields {
            Some(ref s) => s.iter()
                .all(|x| whitelist.iter()
                    .any(|y| *y == *x)
                ),
            None => whitelist.len() == 0
        }
    }

    fn get_formatted_string(&self) -> String {
        format!("{}\t{}\t{}", &self.line[..9], &self.line[10..15].replace(" ", "_"), &self.line[18..])
    }
}

fn read_file(filepath: String) -> Result<BufReader<File>, io::Error> {
    let path = Path::new(&filepath);
    let file = File::open(path)?;
    Ok(BufReader::new(file))
}

fn parse_csv_as_rule_set<R: Read>(buffered_text: BufReader<R>) -> Vec<Rule> {
    let mut rule_set = vec![];
    for line in buffered_text.lines() {
        let rule = Rule::new(&line.unwrap());
        rule_set.push(rule);
    }
    rule_set
}

fn match_lines_to_rules<T: BufRead>(rule_set: &Vec<Rule>, lines: Lines<T>) -> Vec<String> {
    let mut res = vec![];
    for line in lines {
        let marc_field = MarcField::new(&line.unwrap());
        for rule in rule_set {
            if marc_field.is_field(&rule.field) {
                let check = match rule.list_type {
                    ListType::Blacklist => marc_field.blacklist_check(&rule.subfields),
                    ListType::Whitelist => marc_field.whitelist_check(&rule.subfields),
                    ListType::NonExistence => false
                };
                if !check {
                    res.push(marc_field.get_formatted_string());
                }
            }
        }
    }
    res
}

fn order_output(mut output_list: Vec<String>) -> Vec<String> {
    output_list.sort_unstable_by(|a, b| {
        let a_fields = a.split("\t").take(2).collect::<Vec<&str>>();
        let b_fields = b.split("\t").take(2).collect::<Vec<&str>>();
        if a_fields[1] < b_fields[1] {
            Ordering::Less
        } else if a_fields[1] > b_fields[1] {
            Ordering::Greater
        } else {
            if a_fields[0] < b_fields[0] {
                Ordering::Less
            } else if a_fields[0] > b_fields[0] {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        }
    });
    output_list
}

fn print_output(output_list: Vec<String>) {
    for line in output_list {
        println!("{}", line);
    }
}

fn main() {
    let filepath = std::env::args().skip(1).next();
    let file = read_file(filepath.unwrap());
    let rule_set = parse_csv_as_rule_set(file.unwrap());

    let stdin = io::stdin();
    let res_unordered = match_lines_to_rules(&rule_set, stdin.lock().lines());
    let res_ordered = order_output(res_unordered);
    print_output(res_ordered);
}
